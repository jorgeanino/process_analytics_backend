from rest_framework import serializers

from webapp.process.models import Process


class ProcessSerializer(serializers.ModelSerializer):
    is_outlier = serializers.SerializerMethodField()

    def get_is_outlier(self, obj):
        if hasattr(obj, "is_outlier"):
            return obj.is_outlier
        return False

    class Meta:
        model = Process
        fields = [
            "process_id",
            "name",
            "start",
            "end",
            "duration",
            "status",
            "message",
            "is_outlier"
        ]


class ProcessDailyDataSerializer(serializers.ModelSerializer):
    day_duration = serializers.SerializerMethodField()
    formatted_day_duration = serializers.SerializerMethodField()
    start_date = serializers.SerializerMethodField()

    def format_timedelta(self, td):
        days, remainder = divmod(td.total_seconds(), 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)
        return "{} days, {} hours, {} minutes, {} seconds".format(int(days), int(hours), int(minutes), int(seconds))

    def get_day_duration(self, obj):
        return obj.get("day_duration")

    def get_start_date(self, obj):
        return obj.get("start__date").strftime("%Y-%m-%d")

    def get_formatted_day_duration(self, obj):
        return self.format_timedelta(obj.get("day_duration"))

    class Meta:
        model = Process
        fields = [
            "name",
            "day_duration",
            "formatted_day_duration",
            "start_date"
        ]


class ProcessMonthlyDataSerializer(serializers.ModelSerializer):
    error_rate = serializers.SerializerMethodField()
    total_processes = serializers.SerializerMethodField()
    error_processes = serializers.SerializerMethodField()

    def get_error_rate(self, obj):
        return f"{obj.get('error_rate')} %"

    def get_total_processes(self, obj):
        return obj.get("total_processes")

    def get_error_processes(self, obj):
        return obj.get("error_processes")

    class Meta:
        model = Process
        fields = [
            "name",
            "error_rate",
            "total_processes",
            "error_processes"
        ]

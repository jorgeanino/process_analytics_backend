from django.db.models import IntegerField
from django.db.models.expressions import Func


class Epoch(Func):
    template = 'EXTRACT(epoch FROM %(expressions)s)::INTEGER'
    output_field = IntegerField()

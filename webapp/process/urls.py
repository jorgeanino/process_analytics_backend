from django.conf.urls import include, url

from rest_framework import routers

from .views import ProcessViewSet

router = routers.DefaultRouter()
router.register(r'process', ProcessViewSet, basename="process_view_set")

urlpatterns = [
    url(r'^', include(router.urls)),
]

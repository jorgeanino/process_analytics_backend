import io
import csv
from datetime import datetime, timedelta

from django.db import transaction
from django.db.models import (
    ExpressionWrapper,
    F,
    BooleanField,
    IntegerField,
    Sum,
    Count,
    Case,
    When,
    FloatField,
    Window,
    Value,
    Func
)
from django.db.models.functions import PercentRank

from rest_framework import viewsets, status
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import action
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from webapp.pagination import Pagination
from webapp.process.db_functions import Epoch
from webapp.process.models import Process
from webapp.process.serializers import (
    ProcessDailyDataSerializer,
    ProcessMonthlyDataSerializer,
    ProcessSerializer
)
from webapp.process.filters import ProcessFilter
from webapp.process.exceptions import InvalidDateFormatException


class ProcessViewSet(viewsets.ModelViewSet):
    """Process viewset

    / - returns process ata
    daily/ - returns process daily data
    monthly/ - returns process monthly data
    import/ - imports process data from CSV
    """
    queryset = Process.objects.all()
    pagination_class = Pagination
    serializer_class = ProcessSerializer
    lookup_field = 'process_id'
    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter
    )
    filterset_class = ProcessFilter
    http_method_names = ['get', 'post']
    ordering = ('name',)

    def _annotate_outliers(self, queryset):
        # Calculate the difference between end and start in seconds
        queryset = queryset.annotate(
            duration_seconds=Epoch(F('duration'))
        )

        # Calculate the percent rank of each process's duration within its name group
        queryset = queryset.annotate(
            rank=Window(
                expression=PercentRank(),
                partition_by=[F('name')],
                order_by=[F('duration_seconds')]
            )
        )

        # Annotate outliers based on the percent rank
        queryset = queryset.annotate(
            is_outlier=Case(
                # Outliers are below the 2.5th percentile
                When(rank__lt=0.025, then=Value(True)),
                # Outliers are above the 97.5th percentile
                When(rank__gt=0.975, then=Value(True)),
                default=Value(False),
                output_field=BooleanField()
            )
        )

        return queryset

    def _annotate_daily_data(self, queryset):
        """Annotate process daily duration"""

        # Group by name and start date, then annotate with the total duration for each group
        queryset = queryset.values('name', 'start__date') \
            .annotate(day_duration=Sum('duration')) \
            .order_by('name', 'start__date')

        return queryset

    def _annotate_monthly_data(self, queryset):
        """Annotate total processes and error processes for each month"""
        queryset = queryset.values('name', 'start__year', 'start__month').annotate(
            total_processes=Count('process_id'),
            error_processes=Count(Case(When(status="ERROR", then=1))),
        ).annotate(
            error_rate=ExpressionWrapper(
                (F('error_processes') * 100.0) / F('total_processes'),
                output_field=FloatField()
            )
        )
        return queryset

    def _parse_row_data(self, row):
        """Parse row from csv to return ready to serialize information"""
        try:
            start_datetime_str = f"{row[2]} {row[3]}"
            end_datetime_str = f"{row[4]} {row[5]}"

            start_datetime = datetime.strptime(
                start_datetime_str, '%d/%m/%Y %H:%M:%S')
            end_datetime = datetime.strptime(
                end_datetime_str, '%d/%m/%Y %H:%M:%S')

            duration = end_datetime - start_datetime

        except (TypeError, ValueError) as e:
            raise InvalidDateFormatException

        return {
            "process_id": row[0],
            "name": row[1],
            "start": start_datetime,
            "end": end_datetime,
            "duration": duration,
            "status": row[6],
            "message": row[7]
        }

    def _paginated_or_full_response(self, annotation_method, serializer):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = annotation_method(queryset)
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        queryset = annotation_method(queryset)
        serializer = serializer(queryset, many=True)
        return Response(serializer.data)

    @transaction.atomic
    @action(detail=False, methods=['POST'], url_path='import')
    def import_process(self, request):
        """Uploads and imports process CSV file"""
        file = request.data['file']

        if not file.name.endswith('.csv'):
            return Response({"error": "File type not supported"}, status=status.HTTP_400_BAD_REQUEST)

        data_set = file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string)  # Skip header

        process_data = []

        for row in csv.reader(io_string, delimiter=';', quotechar="|"):
            try:
                row_data = self._parse_row_data(row)
            except IndexError as e:
                return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)
            except InvalidDateFormatException:
                return Response({"error": "Invalid date format"}, status=status.HTTP_400_BAD_REQUEST)

            process_data.append(row_data)

        serializer = ProcessSerializer(data=process_data, many=True)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        processes_to_create = [Process(
            **process
        ) for process in validated_data]

        Process.objects.bulk_create(processes_to_create)

        return Response({"message": "Processes were imported successfully"}, status=status.HTTP_201_CREATED)

    def list(self, request):
        return self._paginated_or_full_response(self._annotate_outliers, ProcessSerializer)

    @action(detail=False, methods=['GET'], url_path='daily')
    def process_daily_data(self, request):
        return self._paginated_or_full_response(self._annotate_daily_data, ProcessDailyDataSerializer)

    @action(detail=False, methods=['GET'], url_path='monthly')
    def process_monthly_data(self, request):
        return self._paginated_or_full_response(self._annotate_monthly_data, ProcessMonthlyDataSerializer)

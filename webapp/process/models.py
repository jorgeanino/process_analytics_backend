from django.db import models


class Process(models.Model):
    process_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    start = models.DateTimeField()
    end = models.DateTimeField()
    duration = models.DurationField()
    status = models.CharField(max_length=255)
    message = models.TextField()

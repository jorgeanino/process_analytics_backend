import django_filters

from webapp.process.models import Process


class ProcessFilter(django_filters.FilterSet):

    class Meta:
        model = Process
        fields = ['name']

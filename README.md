# Process Endpoints Documentation

## Overview
The `ProcessViewSet` offers a suite of endpoints dedicated to handling process data. This includes importing process data from CSV files, fetching data aggregated daily and monthly, and much more.

---

## Table of Contents
- [Endpoints](#endpoints)
  - [List Processes](#1-list-processes)
  - [Import Processes from CSV](#2-import-processes-from-csv)
  - [Get Daily Process Data](#3-get-daily-process-data)
  - [Get Monthly Process Data](#4-get-monthly-process-data)
- [Filters](#filters)
- [Ordering](#ordering)
- [Pagination](#pagination)
- [Exceptions](#exceptions)

---

## Endpoints

### 1. List Processes
- **Endpoint:** `/`
- **Method:** `GET`
- **Description:** Retrieves a list of processes annotated for outliers.

### 2. Import Processes from CSV
- **Endpoint:** `/import/`
- **Method:** `POST`
- **Description:** Upload and import process data from a CSV file. The expected format for the CSV includes columns:
  - Process ID
  - Name
  - Start Date
  - Start Time
  - End Date
  - End Time
  - Status
  - Message

  The Date and Time columns should adhere to the `%d/%m/%Y %H:%M:%S` format.

### 3. Get Daily Process Data
- **Endpoint:** `/daily/`
- **Method:** `GET`
- **Description:** Fetches process data aggregated on a daily basis.

### 4. Get Monthly Process Data
- **Endpoint:** `/monthly/`
- **Method:** `GET`
- **Description:** Returns aggregated monthly process data, which includes total and error process counts for each month.

---

## Filters
The `ProcessViewSet` utilizes the `ProcessFilter` class, enabling filtering on the data. Apply these filters by appending them as query parameters to the request URL.

## Ordering
Ordering of returned processes is possible based on their names. Use the `ordering` query parameter and set its value to `name` for ascending order. For descending order, prepend with a `-`, i.e., `-name`.

## Pagination
The `Pagination` class handles the pagination of results. Configure items per page and other pagination settings within the `Pagination` class.

## Exceptions
- **Invalid File Type:** Uploading a non-CSV file results in a `400 Bad Request` with the error message "File type not supported."
- **Invalid Date Format:** If the CSV contains an incorrect date format, it will return a `400 Bad Request` with the error message "Invalid date format."

For successful CSV imports, expect a `201 Created` response, accompanied by the message: "Processes were imported successfully."